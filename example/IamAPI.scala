import com.gitlab.vitrox.boaa.Iam
import com.gitlab.vitrox.boaa.models.iam.LoginToken

object IamAPI {

  /** @param args [username, password]
    */
  def main(args: Array[String]): Unit = {
    implicit var loginToken: LoginToken = try {
      val token = Iam.login(args.head, args(1)).get
      println(s"Successfully logged in. Your session token is: ${token.sessionToken}")
      token
    } catch {
      case _: Exception => throw new Error("Could not login successfully")
    }

    // Refreshes the token
    loginToken = Iam.refreshToken.get

    val validateToken = Iam.validateToken
    if (!validateToken.success) throw new Error("Your token is invalid")
  }

}