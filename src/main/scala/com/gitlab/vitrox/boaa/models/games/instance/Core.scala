/*
 * battle-of-ai-api
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.boaa.models.games.instance

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import com.gitlab.vitrox.boaa.models.Id
import com.gitlab.vitrox.boaa.models.games.{GameBoard, GameInstance, GameState, GameStateType}

case class Core(id: Long,
                @JsonProperty("winning_player") winningPlayer: Option[Long],
                players: Vector[Id],
                @JsonProperty("open_slots") openSlots: Long,
                @JsonProperty("game_state")
                @JsonScalaEnumeration(classOf[GameStateType]) gameState: GameState.GameState,
                @JsonProperty("game_name") gameName: String,
                @JsonProperty("active_player") activePlayer: Long,
                history: Vector[GameBoard[Vector[Vector[Char]]]]) extends GameInstance[Vector[Vector[Char]]]