/*
 * battle-of-ai-api
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.boaa

import java.net.{HttpURLConnection, URL}

import com.gitlab.vitrox.boaa.models.games._
import com.gitlab.vitrox.boaa.models.games.instance.JsonNodeInstance
import com.gitlab.vitrox.boaa.query.{Query, QueryBuilder, QueryFalse}

/**
  * Entry point for the Games API and Games Query API
  * See the official documentation for more information: https://games.battleofai.net/api/
  */
object Games {

  @inline private def constructUrl(endpoint: Any) =
    new URL("https", "games.battleofai.net", 443, s"/api/games/$endpoint")

  def getAll: ListGames =
    mapper.readValue(
      constructGetConnection(constructUrl(""), connection => acceptJson(connection))
        .getInputStream, classOf[ListGames])

  def data[T <: GameInstance[_]](gameId: Long, gameInstanceClass: Class[T]): T =
    mapper.readValue(
      constructGetConnection(constructUrl(gameId), connection => acceptJson(connection))
        .getInputStream, gameInstanceClass)

  def rawData(gameId: Long): String =
      inputStreamToString(constructGetConnection(constructUrl(gameId),
        connection => acceptJson(connection)).getInputStream)

  def nodeData(gameId: Long): JsonNodeInstance =
    mapper.readValue(
      constructGetConnection(constructUrl(gameId), connection => acceptJson(connection))
        .getInputStream, classOf[JsonNodeInstance])

  def create(gameName: String): Boolean = {
    val connection = constructPostConnection(constructUrl("createGame"), connection => sendJson(connection))
    mapper.writeValue(connection.getOutputStream, GameName(gameName))
    connection.getResponseCode == HttpURLConnection.HTTP_OK
  }

  def makeTurn(gameId: Long, turnPayload: TurnPayload[_]): Boolean = {
    val connection = constructPostConnection(constructUrl(s"$gameId/makeTurn"),
      connection => sendJson(connection))
    mapper.writeValue(connection.getOutputStream, turnPayload)
    connection.getResponseCode == HttpURLConnection.HTTP_OK
  }

  def makeTurn(gameId: Long, turn: Any)(implicit playerData: PlayerData): Boolean =
    makeTurn(gameId, TurnPayload(playerData, turn))

  def makeTurn(gameId: Long, turn: Any, playerId: Long, token: String): Boolean =
    makeTurn(gameId, turn)(PlayerData(playerId, token))

  def registerPlayer(gameId: Long)(implicit playerData: PlayerData): Boolean = {
    val connection = constructPostConnection(constructUrl(s"$gameId/registerPlayer"),
      connection => sendJson(connection))
    mapper.writeValue(connection.getOutputStream, playerData)
    connection.getResponseCode == HttpURLConnection.HTTP_OK
  }

  def registerPlayer(gameId: Long, playerId: Long, token: String): Boolean =
    registerPlayer(gameId)(PlayerData(playerId, token))

  @inline def query: QueryBuilder[QueryFalse, QueryFalse, QueryFalse, QueryFalse] = Query.games

}
