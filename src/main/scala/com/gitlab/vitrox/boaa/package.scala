/*
 * battle-of-ai-api
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox

package object boaa {
  import com.fasterxml.jackson.databind.ObjectMapper
  import com.fasterxml.jackson.module.scala.DefaultScalaModule

  //Needed for Jackson JSON parser
  val mapper = new ObjectMapper()
  mapper.registerModule(DefaultScalaModule)

  import java.net.{HttpURLConnection, URL}
  import javax.net.ssl.HttpsURLConnection

  @inline def constructConnection(url: URL, apply: HttpsURLConnection => Unit): HttpsURLConnection = {
    val connection = url.openConnection().asInstanceOf[HttpURLConnection].asInstanceOf[HttpsURLConnection]
    apply(connection)
    connection.connect()
    connection
  }

  @inline def acceptJson(connection: HttpURLConnection): Unit = {
    connection.setRequestProperty("accept", "application/json")
  }

  @inline def sendJson(connection: HttpURLConnection): Unit = {
    connection.setRequestProperty("Content-Type", "application/json")
    connection.setDoOutput(true)
  }

  @inline def constructGetConnection(url: URL, apply: HttpsURLConnection => Unit): HttpsURLConnection  = {
    constructConnection(url, connection => {
      connection.setRequestMethod("GET")
      apply(connection)
    })
  }

  @inline def constructPostConnection(url: URL, apply: HttpsURLConnection => Unit): HttpsURLConnection = {
    constructConnection(url, connection => {
      connection.setRequestMethod("POST")
      apply(connection)
    })
  }

  @inline def postJsonInOut(url: URL, apply: HttpsURLConnection => Unit = _ => {}): HttpsURLConnection =
    constructPostConnection(url, connection => {
      sendJson(connection)
      acceptJson(connection)
      apply(connection)
    })

  import java.io.InputStream
  import scala.io.Source
  @inline def inputStreamToString(inputStream: InputStream): String =
    Source.fromInputStream(inputStream).mkString

  import com.gitlab.vitrox.boaa.models.iam.RequestSuccess
  @inline private def receiveRequestSuccess(connection: HttpsURLConnection): RequestSuccess =
    mapper.readValue(connection.getInputStream, classOf[RequestSuccess])

  @inline def postJsonReceiveRequestSuccess(url: URL, post: Any): RequestSuccess = {
    val connection = postJsonInOut(url)
    mapper.writeValue(connection.getOutputStream, post)
    receiveRequestSuccess(connection)
  }

}