/*
 * battle-of-ai-api
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.boaa

import java.net.{HttpURLConnection, URL}

import com.gitlab.vitrox.boaa.models.iam._

/**
  * Entry point for the Iam API
  * See the official documentation for more information: https://iam.battleofai.net/api/
  */
object Iam {

  @inline private def constructUrl(endpoint: Any) =
    new URL("https", "iam.battleofai.net", 443, s"/api/iam/$endpoint")

  def forgotPassword(email: String): Boolean = {
    val connection = constructPostConnection(constructUrl("forgotPassword"), connection => {
      sendJson(connection)
      connection.setDoOutput(true)
    })
    mapper.writeValue(connection.getOutputStream, EmailPayload(email))
    connection.getResponseCode == HttpURLConnection.HTTP_OK
  }

  def getUserByID(userId: Long, token: String, sessionToken: String): UserData =
    getUserByID(LoginToken(userId, token, sessionToken))

  def getUserByID(implicit loginToken: LoginToken): UserData = {
    val connection = postJsonInOut(constructUrl(s"getUserByID/${loginToken.userId}"))
    mapper.writeValue(connection.getOutputStream, loginToken)
    mapper.readValue(connection.getInputStream, classOf[UserData])
  }

  import javax.net.ssl.HttpsURLConnection
  @inline private def receiveLoginToken(connection: HttpsURLConnection): Option[LoginToken] = {
    val result = mapper.readValue(connection.getInputStream, classOf[LoginToken])
    if (result.userId == 0 && result.token == null && result.sessionToken == null) None
    else Some(result)
  }

  def login(username: String, password: String): Option[LoginToken] = {
    val connection = postJsonInOut(constructUrl("login"))
    mapper.writeValue(connection.getOutputStream, LoginPayload(username, password))
    receiveLoginToken(connection)
  }

  def refreshToken(userId: Long, token: String, sessionToken: String): Option[LoginToken] =
    refreshToken(LoginToken(userId, token, sessionToken))

  def refreshToken(implicit loginToken: LoginToken): Option[LoginToken] = {
    val connection = postJsonInOut(constructUrl("refreshToken"))
    mapper.writeValue(connection.getOutputStream, loginToken)
    receiveLoginToken(connection)
  }

  def register(username: String, email: String, password: String, newsletter: Boolean): RequestSuccess =
    postJsonReceiveRequestSuccess(constructUrl("register"),
      RegisterPayload(username, email, password, newsletter))

  def resetPassword(emailToken: String, newPassword: String): RequestSuccess =
    postJsonReceiveRequestSuccess(constructUrl("resetPassword"),
      ResetPasswordPayload(emailToken, newPassword))

  def updateUser(oldPassword: String,
                 email: String,
                 newPassword: String,
                 newPassword2: String,
                 newsletter: Boolean)(implicit token: LoginToken): RequestSuccess =
    postJsonReceiveRequestSuccess(constructUrl("updateUser"),
      UpdateUserPayload(token, oldPassword, email, newPassword, newPassword2, newsletter))

  def updateUser(userId: Long,
                 token: String,
                 sessionToken: String,
                 oldPassword: String,
                 email: String,
                 newPassword: String,
                 newPassword2: String,
                 newsletter: Boolean): RequestSuccess =
    updateUser(oldPassword, email, newPassword, newPassword2, newsletter)(LoginToken(userId, token, sessionToken))

  def validateToken(implicit loginToken: LoginToken): RequestSuccess =
    postJsonReceiveRequestSuccess(constructUrl("validateToken"), loginToken)

  def validateToken(userId: Long, token: String, sessionToken: String): RequestSuccess =
    validateToken(LoginToken(userId, token, sessionToken))

  def verifyEmail(emailToken: String): RequestSuccess =
    postJsonReceiveRequestSuccess(constructUrl("verifyEmail"), EmailTokenPayload(emailToken))

}
