/*
 * battle-of-ai-api
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.boaa.query

import java.net.URL

import com.gitlab.vitrox.boaa.models.games.GameState.GameState
import com.gitlab.vitrox.boaa.models.games.ListGames
import com.gitlab.vitrox.boaa.{acceptJson, constructGetConnection, mapper}

/**
  * Type-Safe builder for Games Query API
  */
class QueryBuilder[QA, QB, QC, QD] private(payload: String) {
  @inline private def addArgument(name: String, action: Any): String =
    (if (payload.isEmpty) '?' else payload + '&') + name + '=' + action

  def gameState(gameState: GameState)(implicit e: QA =:= QueryFalse) =
    new QueryBuilder[QueryTrue, QB, QC, QueryTrue](addArgument("game_state", gameState))

  def gameName(name: String)(implicit e: QB =:= QueryFalse) =
    new QueryBuilder[QA, QueryTrue, QC, QueryTrue](addArgument("game_name", name))

  def userIds(ids: Long*)(implicit e: QC =:= QueryFalse) =
    new QueryBuilder[QA, QB, QueryTrue, QueryTrue](addArgument("user_ids", ids.head)) //TODO multiple ids

  def execute()(implicit e: QD =:= QueryTrue): ListGames = {
    val connection = constructGetConnection(
      new URL("https", "games.battleofai.net", 443, s"/api/games/$payload"),
      connection => acceptJson(connection))
    mapper.readValue(connection.getInputStream, classOf[ListGames])
  }
}

object QueryBuilder {
  def apply() = new QueryBuilder[QueryFalse, QueryFalse, QueryFalse, QueryFalse]("")
}