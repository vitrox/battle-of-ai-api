organization := "com.gitlab.vitrox"
scalaVersion := "2.12.6"

name := "battle-of-ai-api"
version := "0.2.1"

libraryDependencies ++= Seq(
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.9.7",
  "org.scalatest" %% "scalatest" % "3.0.5" % Test,
)