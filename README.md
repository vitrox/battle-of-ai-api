# Battle of AI API (Scala)

Implementation of the Battle of AI API for Scala, originally written in [Python](https://github.com/TheMorpheus407/BattleOfAI).

Battle Of AI is an community driven project initiated by the german youtuber 
[TheMorpheus407](https://www.youtube.com/user/TheMorpheus407).

### Visit [battleofai.net](https://battleofai.net/) for more information

## Features

### [Iam API 0.1](https://iam.battleofai.net/api/)

### [Games API 0.1](https://games.battleofai.net/api/)

### Games Query API

Via Type-Safe builder
    
### Serialization and Deserialization

With [Jackson](https://github.com/FasterXML/jackson) JSON parser

Objects for sending and receiving JSON payloads to/from the API.

Objects for the following games:
+ [Core](https://github.com/ac1235/core)

Function to provide your own object for receiving a game instance.

## Installation

Use JitPack to add this project into your build.
   
#### sbt
   
Add the JitPack repository in your build.sbt at the end of resolvers:
   
```scala
resolvers += "jitpack" at "https://jitpack.io"
```
   
Add the dependency:

```scala
libraryDependencies += "com.gitlab.vitrox" % "battle-of-ai-api" % version
```

Other JVM languages than Scala were not tested.
   
See [this](https://jitpack.io/#com.gitlab.vitrox/battle-of-ai-api) guide on JitPack for more information or click on the badge.

## Example usage

### Iam API

```scala
// Login and receives the login token
/** @param args [username, password]
  */
  def main(args: Array[String]): Unit = {
    implicit var loginToken: LoginToken = try {
      val token = Iam.login(args.head, args(1)).get
      println(s"Successfully logged in. Your session token is: ${token.sessionToken}")
      token
    } catch {
      case _: Exception => throw new Error("Could not login successfully")
    }
  }
```

### Games API

```scala
// Prints the ids of all games
for (game <- Games.getAll.games) {
  println(game.id)
}

// Gets the game with id 0 in a Core object
Games.data(0, classOf[Core])
```

### Games Query API

```scala
// Gets all core games with the game state of waiting
Games.query.gameName("Core").gameState(GameState.Waiting).execute()
```